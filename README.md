# CPU Venue Booking System 

## Background
To make the booking procedures more convenient, CPU is going to set up a booking system for staff to book the venue in school days. 
In the past, this school faces 2 major problems when booking a venue:

1. manlipulation of each booking is not efficient
2. hard to review the booking timetable 

In order to solve the problems, a new system would be developed with the following requirement:

## Objectives
As to solve the problems mentioned before, there are 2 major goals we want to achieve, which are:
1. Checking the timetable of the booking venue 
    - Searching the booking information required by the user
    - Sorting the booking information
    - Criteria to be considered for searching and sorting: 
        - time, booker, venue
2. Editing the booking schedule 
    - Making a booking
    - Editing the booking information
    - Canceling a booking

## Description
The program will ask for a request of any action, including booking venue and viewing the booking timetable in the ``main program``: ![main](images/sba - Main.png)

Afterwards, the user can ``add``, ``edit`` or ``cancel`` any of his bookings under the function of booking venue, and also viewing the booking timetable sort by different criteria under the function viewing booking timetable. 
##### Add booking function:
![addBooking](images/sba - addBooking.png)
##### Edit booking function
![editBooking](images/sba - editBooking.png)
##### Cancel booking function
![cancelBooking](images/sba - cancelBooking.png)

The system is entirely written by ``C++`` and it make use of the ``Command-Line Interface(CLU)`` to process input and output action. User can operate the system by a ``executable file``. Data will be stored in ``text file`` after the system is closed. 

## Main features
##### 1. Data verification and validation
In order to ensure the user providing reasonable and acceptable input in the system, a code is written to check the inputted value. Consider the following situation: 
```
Welcome to the CPU booking system!
What can I help you with?
1. New booking--------(enter 1)
2. Edit booking info--(enter 2)
3. Cancel booking-----(enter 3)
4. Timetable viewing--(enter 4)
Exit at any time:  type "exit*"
--Please enter the command index for further action--
```
In this front page, the user is required to type either a integer in the range of ``1-4`` or ``exit*``. To check if the user is only giving the computer valid value, the following program ``integralValidator()`` is constructed. 
```cpp
int integralValidator(int min, int max) {
    int i;
    string input;
    getline(cin, input); // collect user's input
    input.erase(input.find_last_not_of(" \n\r\t")+1);
    while (true) {
        if (input == "exit*") return -404;           // exit the system
        while (input.length() > log10(max) + 1) {    // conducting the length check
            cout << "Invalid input, please enter an index again:\n";
            getline(cin, input);
            input.erase(input.find_last_not_of(" \n\r\t")+1);
        }
        for (i = 0; i < input.length(); i++) {
            if (input[i] < 48 || input[i] > 57) {    // conducting the type check
                i = -100;
                cout << "Not an integer";
                break;
            }
        }
        if (i != -100) {
            if (stoi(input) >= min && stoi(input) <= max) return stoi(input); // conducting the range check
            else cout << "Out of range";
        }
        cout << ", please enter an index again:\n";
        getline(cin, input);
        input.erase(input.find_last_not_of(" \n\r\t")+1);
    }      
}
```
By using the ``integralValidator()``, we can ensure that the user's input is an __integer__ and it __lies between the minimum and the maximum value inclusively__. 

##### 2. Class implementation
Using an organized and clear structure to store and use data is important, espically for a large group of data in our case. Therefore, a class named ``venue`` is constructed to store all the booking information when the system is running.
```cpp
class venue {
    public:
        string username[11]; // username
        string purpose[11];  // purpose
        //...//
};
venue cyberHub, innoLab, steamCave, ideaLounge, mmlc, gpRoom, artRoom, musicRoom, lectureTheatre;
```
For each of the venue avaliable to book, a class named by the venun is initalized. Afterwards, the program will get the data form the text file and store in the classes.
```cpp
// inside the class venue{}
void input (ifstream& read) {         // get the data for each venue from the text file
    string text;
    int i = 0;
    while (true) {
        read >> text;
        if (text == "``") break;
        username[i] = text;           // get the username
        i++;
    }
    read.ignore(10,'\n');
    i = 0;
    while (getline (read, text)) {
        if (text == "```") break;
        purpose[i] = text;            // get the purpose
        i++;
    }
}      
```
When the booking information is updated after running the system, the program will save the new data from the classes to the text file.
```cpp
// inside the class venue{}
void output(ofstream& write) {         // save the data of each venue into the text file
    int i = 0;
    for (i = 0; i < 11; i++) {
        write << username[i] << " ";   // save the username
    }
    write << "``" << endl;
    for (i = 0; i < 11; i++) {
        write << purpose[i] << endl;   // save the purpose
    }
    write << "```" << endl;            
}
```
In the file input and outtput process, ``ifstream`` and ``ofstream`` is used to take or put data between the classes and the text file. In addition, due to the text file has organized the ``purpose`` and the ``username`` in the saving process, another variable ``time`` is not necessary to set up to store for each booking.

## Limitation
In the current stage, the system is only avaliable to book the venue for a specific day. Muti-date booking functions have not been developed. In the future, the booking systme will be further developed which is avaliable to book the venue in different days.

## Appendix- flowchart
- [ ] [main program](images/sba - Main.png)
- [ ] [addBooking function](images/sba - addBooking.png)
- [ ] [cancelBooking function](images/sba - cancelBooking.png)
- [ ] [editBooking function](images/sba - editBooking.png)
- [ ] [read function](images/sba - read.png)
- [ ] [save function](images/sba - save.png)
- [ ] [view function](images/sba - view.png)


## Authors and acknowledgment
Choy Tsz Yeung from Carmel Pak U Secondary School


## Project status
- [ ] Status 1: adding new booking function is done
- [ ] Status 2: editing booking information is done
```
others functions are still under development
```
