#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <cmath>
#include "functions/class.h"
#include "functions/global.cpp"
#include "functions/inputData.cpp"
#include "functions/deleteData.cpp"
using namespace std; 

// function and variable initalization
int newBooking();
int editBooking();
int cancelBooking();
int viewBooking();

vector<string> purposeText;
vector<int> timeIndex,placeIndex;

int main() {
    int key, status;
    
    while (true) {
        ifstream read("../database/data.txt");   // getting the data from the text file
        cyberHub.input(read);
        innoLab.input(read);
        steamCave.input(read);
        ideaLounge.input(read);
        mmlc.input(read);
        gpRoom.input(read);
        artRoom.input(read);
        musicRoom.input(read);
        lectureTheatre.input(read);
        read.close();
    
        cout << "Welcome to the CPU booking system!\nWhat can I help you with?\n"
        << "1. New booking--------(enter 1)\n2. Edit booking info--(enter 2)\n3. Cancel booking-----(enter 3)\n"
        << "4. Timetable viewing--(enter 4)\nExit at any time:  type \"exit*\"\n" << "--Please enter the command index for further action--\n";
        key = integralValidator(1,5);            // choosing the next action
        if (key == -404) break;

        switch (key) {
            case 1:
                if (newBooking() < 2) break;     // make a new booking 
                break;
            case 2:
                if (editBooking() < 2) break;    // edit booking information
                break;
            case 3:
                if (cancelBooking() < 2) break;  // delete a booking 
                break;
            case 4:
                if (viewBooking() < 2) break;    // view the booking status (not yet developed)
                break;
        }

        ofstream write("../database/data.txt");  // input the data to the text file
        cyberHub.output(write);
        innoLab.output(write);
        steamCave.output(write);
        ideaLounge.output(write);
        mmlc.output(write);
        gpRoom.output(write);
        artRoom.output(write);
        musicRoom.output(write);
        lectureTheatre.output(write);
        write.close();  
    }

    return 0;
}


int newBooking() {
    int place, period;
    string reason, person, con; 

    place = getVenue("all", 0);              // getting the venue
    //int *placeANDperiod = getTime(place);    // getting the time
    period = getTime(place);
    person = getUrnm();                      // getting the username
    if (person == "exit*") return 0;         // exit command
    reason = getReason();                    // getting the reason
    if (reason == "exit*") return 0;         // exit command
    con = confirm(person, place, period, reason);   // send a confirmation message to the user
    return 1;
}


int editBooking() {
    string con, time[11] = {"8:25-9:05  ", "9:05-9:45  ", "9:45-10:25 ", "10:40-11:20", "11:20-12:00", "12:00-12:40", "12:40-13:45", "13:45-14:25", "14:25-15:00", "15:00-15:35", "15:35-18:00"},
    ven[9] = {"Cyber Hub", "Inno Lab", "STEAM Cave", "Idea Lounge", "MMLC", "GP Room", "Art Room", "Music Room", "Lecture Theatre"},
    changeItem[3] = {"VENUE", "TIME SLOT", "PURPOSE"};
    int place, period, keyBooking, keyChange, count = 1;
    //int *placeANDperiod;
    string reason, person;
    timeIndex.clear();
    placeIndex.clear();
    purposeText.clear();

    person = callData(count, placeIndex, timeIndex, purposeText);  // getting the booking information which is booked by the person
    if (count == 1) {
        cout << "- - - You have no bookings yet - - -\nReturning to the main page......\n\n";
        return 0;
    }
    cout << "Which booking would you like to edit?" << endl;       // select the booking to be edited
    keyBooking = integralValidator(1, count-1) - 1;
    
    place = placeIndex[keyBooking];                                // getting the information of the selected booking
    period = timeIndex[keyBooking]; 
    reason = purposeText[keyBooking];  

    cout << "Booking " << keyBooking + 1 << " is chosen . . .\n\n" << "Booker: " << person << "\nVenue: " << ven[place]
    << "\nBooking time: " << time[period] << "\nPurpose: " << reason << "\n\n";

    cout << "Which information would you like to change?\nVenue-(1)   Time slot-(2)   Purpose-(3)\n";
    keyChange = integralValidator(1,3) - 1;                         // choosing the next action
    cout << "You have chosen to change the [ " << changeItem[keyChange] << " ]\n";

    switch (keyChange + 1) {
        case 1:
            place = getVenue("selected", period);        // choosing a new place
            //placeANDperiod[0] = place;
            break;
        case 2: {
            period = getTime(place);                    // choosing a new time slot
            break;
        } 
        case 3:
            reason = getReason();                                   // entering a new reason
            //placeANDperiod[0] = place + 1;
            break;
    }
    con = confirm(person, place, period, reason);  // send a confirmation message to the user
    if (con == "y" && reason == purposeText[keyBooking]) deleteData(timeIndex[keyBooking], placeIndex[keyBooking]+1);        
    return 0;
}


int cancelBooking() {
    cout << "Sorry, the current function is not developed\nReturning to the main page......\n\n"; // to be developed 
    return 0;
}


int viewBooking() {
    cout << "Sorry, the current function is not developed\nReturning to the main page......\n\n";  // to be developed
    return 0;
}