string getUrnm() {
    string person;
    cout << "What is your name?\n";
    getline(cin, person);
    person.erase(person.find_last_not_of(" \n\r\t")+1);
    return person;
}

string getReason() {
    string reason;
    cout << "What is the purpose of booking?\n";
    getline(cin, reason);
    reason.erase(reason.find_last_not_of(" \n\r\t")+1);
    return reason;
}

int getVenue(string select, int time) { 
    int place, count = 1;
    vector <int> freeVenue;
    freeVenue.clear();
    cout << "Which venue would you like to book?\n";
    if (select == "all") {
        cout << "Cyber Hub(1)  Inno Lab(2)  STEAM Cave(3)  "
        << "Idea Lounge(4)  MMLC(5)  GP Room(6)  Art Room(7)  Music Room(8)  Lecture Theatre(9)\n";
        return integralValidator(1,9) - 1;
    } else if (select == "selected") {
        //cout << " InputData " << time << endl;
        cyberHub.getFreeVenue(time,1,count,freeVenue);
        innoLab.getFreeVenue(time,2,count,freeVenue);
        steamCave.getFreeVenue(time,3,count,freeVenue);
        ideaLounge.getFreeVenue(time,4,count,freeVenue);
        mmlc.getFreeVenue(time,5,count,freeVenue);
        gpRoom.getFreeVenue(time,6,count,freeVenue);
        artRoom.getFreeVenue(time,7,count,freeVenue);
        musicRoom.getFreeVenue(time,8,count,freeVenue);
        lectureTheatre.getFreeVenue(time,9,count,freeVenue);
        cout << endl;
    }
    return freeVenue[integralValidator(1,count) - 1] - 1;
}

int getTime(int& place) {    //enum
    int period;
    //int static output[2];
    vector< int > freeTimeSlog;
    string time[11] = {"8:25-9:05", "9:05-9:45", "9:45-10:25", "10:40-11:20", "11:20-12:00", "12:00-12:40", "12:40-13:45", "13:45-14:25", "14:25-15:00", "15:00-15:35", "15:35-18:00"};

    cout << "Avaliable time for ";
    switch (place + 1) {
        case 1:
            cout << "Cyber Hub:\n";
            for (int i=0;i<11;i++) {
                if (cyberHub.username[i] == "null") {
                    freeTimeSlog.push_back(i);
                    cout << time[i] << "(" << freeTimeSlog.size() << ")   ";
                }
            }
            break;
        case 2:
            cout << "Inno lab:\n";
            for (int i=0;i<11;i++) {
                if (innoLab.username[i] == "null") {
                    freeTimeSlog.push_back(i);
                    cout << time[i] << "(" << freeTimeSlog.size() << ")   ";
                }
            }
            break;
        case 3:
            cout << "STEAM Cave:\n";
            for (int i=0;i<11;i++) {
                if (steamCave.username[i] == "null") {
                    freeTimeSlog.push_back(i);
                    cout << time[i] << "(" << freeTimeSlog.size() << ")   ";
                }
            }
            break;
        case 4:
            cout << "Idea Lounge:\n";
            for (int i=0;i<11;i++) {
                if (ideaLounge.username[i] == "null") {
                    freeTimeSlog.push_back(i);
                    cout << time[i] << "(" << freeTimeSlog.size() << ")   ";
                }
            }
            break;
        case 5:
            cout << "MMLC:\n";
            for (int i=0;i<11;i++) {
                if (mmlc.username[i] == "null") {
                    freeTimeSlog.push_back(i);
                    cout << time[i] << "(" << freeTimeSlog.size() << ")   ";
                }
            }
            break;
        case 6:
            cout << "GP Room:\n";
            for (int i=0;i<11;i++) {
                if (gpRoom.username[i] == "null") {
                    freeTimeSlog.push_back(i);
                    cout << time[i] << "(" << freeTimeSlog.size() << ")   ";
                }
            }
            break;
        case 7:
            cout << "Art Room:\n";
            for (int i=0;i<11;i++) {
                if (artRoom.username[i] == "null") {
                    freeTimeSlog.push_back(i);
                    cout << time[i] << "(" << freeTimeSlog.size() << ")   ";
                }
            }
            break;
        case 8:
            cout << "Music Room:\n";
            for (int i=0;i<11;i++) {
                if (musicRoom.username[i] == "null") {
                    freeTimeSlog.push_back(i);
                    cout << time[i] << "(" << freeTimeSlog.size() << ")   ";
                }
            }
            break;
        case 9:
            cout << "Lecture Theatre:\n";
            for (int i=0;i<11;i++) {
                if (lectureTheatre.username[i] == "null") {
                    freeTimeSlog.push_back(i);
                    cout << time[i] << "(" << freeTimeSlog.size() << ")   ";
                }
            }
            break;
        default:
            cout << "this is default";
    } 
    if(freeTimeSlog.size() == 0) {
        cout << "No free space available\nPlease choose the venue again...\n\n";
        place = getVenue("all", 0);
        return getTime(place);
    }

    //output[0] = place;       
    cout << "\nWhich time slot would you like to book? ";
    period = integralValidator(1,freeTimeSlog.size()) - 1;
    //output[1] = freeTimeSlog[period-1];
    return period;
}

string confirm(string person, int place, int period, string reason) {
    string con;
    //vector< int > freeTimeSlog;
    string time[11] = {"8:25-9:05", "9:05-9:45", "9:45-10:25", "10:40-11:20", "11:20-12:00", "12:00-12:40", "12:40-13:45", "13:45-14:25", "14:25-15:00", "15:00-15:35", "15:35-18:00"},
    ven[9] = {"Cyber Hub", "Inno Lab", "STEAM Cave", "Idea Lounge", "MMLC", "GP Room", "Art Room", "Music Room", "Lecture Theatre"};
    
    cout << "\n~{Confirm booking information}~\nBooker: " << person << "\nVenue: " << ven[place]
    << "\nBooking time: " << time[period] << "\nPurpose: " << reason << "\nConfirm booking? (y/n) ";
    
    
    getline(cin, con);
    //con.erase(con.find_last_not_of(" \n\r\t")+1);
    while (con != "y" && con != "n") {
        cout << "Invalid input, please enter again:";
        getline(cin, con);
        //con.erase(con.find_last_not_of(" \n\r\t")+1);
    }


    if (con == "y") {
        cout << "\nBooking confirmed successfully!\nReturning to the main page......\n\n";
        switch (place + 1) {
            case 1:
                cyberHub.username[period] = person;
                cyberHub.purpose[period] = reason;
                //cout << period << " " << cyberHub.username[period] << " " << cyberHub.purpose[period] << endl;
                break;
            case 2:
                innoLab.username[period] = person;
                innoLab.purpose[period] = reason; 
                //cout << period << " " <<innoLab.username[period] << " " << innoLab.purpose[period] << endl;
                break;
            case 3:
                steamCave.username[period] = person;
                steamCave.purpose[period] = reason;
                //cout << period << " " << cyberHub.username[period] << " " << cyberHub.purpose[period] << endl;
                break;
            case 4:
                ideaLounge.username[period] = person;
                ideaLounge.purpose[period] = reason; 
                //cout << period << " " <<innoLab.username[period] << " " << innoLab.purpose[period] << endl;
                break;
            case 5:
                mmlc.username[period] = person;
                mmlc.purpose[period] = reason;
                //cout << period << " " << cyberHub.username[period] << " " << cyberHub.purpose[period] << endl;
                break;
            case 6:
                gpRoom.username[period] = person;
                gpRoom.purpose[period] = reason; 
                //cout << period << " " <<innoLab.username[period] << " " << innoLab.purpose[period] << endl;
                break;
            case 7:
                artRoom.username[period] = person;
                artRoom.purpose[period] = reason;
                //cout << period << " " << cyberHub.username[period] << " " << cyberHub.purpose[period] << endl;
                break;
            case 8:
                musicRoom.username[period] = person;
                musicRoom.purpose[period] = reason; 
                //cout << period << " " <<innoLab.username[period] << " " << innoLab.purpose[period] << endl;
                break;
            case 9:
                lectureTheatre.username[period] = person;
                lectureTheatre.purpose[period] = reason;
                //cout << period << " " << cyberHub.username[period] << " " << cyberHub.purpose[period] << endl;
                break;
        }          
    } else if (con == "n") {
        cout << "\nBooking is cancelled\nReturning to the main page......\n\n";
    }
    return con;
}

// write an exit function to quit the booking system
void exit() {
    cout << "Thank you for using the booking system!\n";
    cout << "Have a nice day!\n";
    cout << "Exiting......\n";
    exit(0);
}

