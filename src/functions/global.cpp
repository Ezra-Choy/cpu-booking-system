int checkExit(string text);
int integralValidator(int min, int max) {
    int i;
    string input;
    getline(cin, input);
    input.erase(input.find_last_not_of(" \n\r\t")+1);
    while (true) {
        if (input == "exit*") return -404;
        while (input.length() > log10(max) + 1) {
            cout << "Invalid input, please enter an index again:\n";
            getline(cin, input);
            input.erase(input.find_last_not_of(" \n\r\t")+1);
        }
        for (i = 0; i < input.length(); i++) {
            if (input[i] < 48 || input[i] > 57) {
                i = -100;
                cout << "Not an integer";
                break;
            }
        }
        if (i != -100) {
            if (stoi(input) >= min && stoi(input) <= max) return stoi(input);
            else cout << "Out of range";
        }
        cout << ", please enter an index again:\n";
        getline(cin, input);
        input.erase(input.find_last_not_of(" \n\r\t")+1);
    }      
}

int checkExit(string text) {
    if (text == "exit*") {
        cout << "Are you sure you want to exit?\n" << "1. Yes\n2. No\n";
        return 2 - integralValidator(1,2);
    }
    else return 0;
}

string callData(int& count, vector<int>& placeIndex, vector<int>& timeIndex, vector<string>& purposeText) {
    string person;
    cout << "What is your name?" << endl;
    cin >> person;
    cin.ignore(10,'\n');
    cout << "Your bookings are shown below:" << endl;
    count = cyberHub.getBookedData(person, 0, count, placeIndex, timeIndex, purposeText);
    count = innoLab.getBookedData(person, 1, count, placeIndex, timeIndex, purposeText);
    count = steamCave.getBookedData(person, 2, count, placeIndex, timeIndex, purposeText);
    count = ideaLounge.getBookedData(person, 3, count, placeIndex, timeIndex, purposeText);
    count = mmlc.getBookedData(person, 4, count, placeIndex, timeIndex, purposeText);
    count = gpRoom.getBookedData(person, 5, count, placeIndex, timeIndex, purposeText);
    count = artRoom.getBookedData(person, 6, count, placeIndex, timeIndex, purposeText);
    count = musicRoom.getBookedData(person, 7, count, placeIndex, timeIndex, purposeText);
    count = lectureTheatre.getBookedData(person, 8, count, placeIndex, timeIndex, purposeText);
    return person;
}