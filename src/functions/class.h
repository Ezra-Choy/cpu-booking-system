#include <iostream>
#include <string>
#include <vector>
#include <fstream>
using namespace std;

// vector<string> purposeText;
// vector<int> timeIndex,placeIndex;
class venue {
    public:
        string username[11]; // username
        string purpose[11];  // purpose

        void input (ifstream& read) {         // get the data from the text file
            string text;
            int i = 0;
            while (true) {
                read >> text;
                if (text == "``") break;
                username[i] = text;
                i++;
            }
            read.ignore(10,'\n');
            i = 0;
            while (getline (read, text)) {
                if (text == "```") break;
                purpose[i] = text;
                i++;
            }
        }          

        void output(ofstream& write) {         // save the data into the text file
            int i = 0;
            for (i = 0; i < 11; i++) {
                write << username[i] << " ";
            }
            write << "``" << endl;
            for (i = 0; i < 11; i++) {
                write << purpose[i] << endl;
            }
            write << "```" << endl;            
        }

        int getBookedData(string name, int place, int count, vector<int>& placeIndex, vector<int>& timeIndex, vector<string>& purposeText) {
            int i = 0;
            const string time[11] = {"8:25-9:05  ", "9:05-9:45  ", "9:45-10:25 ", "10:40-11:20", "11:20-12:00", "12:00-12:40", "12:40-13:45", "13:45-14:25", "14:25-15:00", "15:00-15:35", "15:35-18:00"},
            venSpc[9] = {"Cyber Hub       ", "Inno Lab        ", "STEAM Cave      ", "Idea Lounge     ", "MMLC            ", "GP Room         ", "Art Room        ", "Music Room      ", "Lecture Theatre"};
            //const string purpose[11] = {"Class", "Clas", "Class", "hee", "Class", "Clas", "Clas", "Class", "Class", "Class", "Clas"};
            for (i = 0; i < 11 ; i++) {
                //cout << i << purpose[i] << " " << name;
                if (username[i] == name) {
                    placeIndex.push_back(place);
                    //cout << "place";
                    timeIndex.push_back(i);
                    //cout << "bye";
                    purposeText.push_back(purpose[i]);
                    //cout << "pushed";
                    if (count < 10) cout << " ";
                    cout << count << "._  Venue: " << venSpc[placeIndex[count-1]] << "|  Booking time: " << time[timeIndex[count-1]]  << "  |  Purpose: " << purpose[i] << endl;
                    count++;
                }
            }
            return count;
        }

        void getFreeVenue(int time, int place, int &count, vector<int>& freeVenue) {
            string ven[9] = {"Cyber Hub", "Inno Lab", "STEAM Cave", "Idea Lounge", "MMLC", "GP Room", "Art Room", "Music Room", "Lecture Theatre"};
            //cout << username[time] << "    " << time << endl;
            if (username[time] == "null") {
                cout << ven[place - 1] << "(" << count << ")  ";
                freeVenue.push_back(place);
                count++;
            }
        }
};

venue cyberHub, innoLab, steamCave, ideaLounge, mmlc, gpRoom, artRoom, musicRoom, lectureTheatre;

class bookedData {
    public:
        
};